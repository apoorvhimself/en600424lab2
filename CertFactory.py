def getPrivateKeyForAddr(addr):
    # ignoring addr, always return the same thing
    address = "/Users/itsApoorv/playground-fall-2016/src/Keys/Client/PrivateKey"
    with open(address) as f:
      return f.read()

def getCertsForAddr(addr):
    chain = []
    addressCert = "/Users/itsApoorv/playground-fall-2016/src/Keys/Client/Cert.cert"
    addressCACert = "/Users/itsApoorv/playground-fall-2016/src/Keys/Client/CA_Cert.cert"
    with open(addressCert) as f:
      chain.append(f.read())
    with open(addressCACert) as f:
      chain.append(f.read())
    return chain

def getRootCert():
	return open("/Users/itsApoorv/playground-fall-2016/src/Keys/20164_signed.cert").read()
